package com.ericjohnson.eventbustraining

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.btnOpenSecondActivity
import kotlinx.android.synthetic.main.activity_main.tvTextField
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode.ASYNC
import org.greenrobot.eventbus.ThreadMode.MAIN
import org.greenrobot.eventbus.ThreadMode.POSTING

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction().add(R.id.fragmentContainer, InputTextFragment()).commit()

        btnOpenSecondActivity.setOnClickListener {
            startActivity(Intent(this, SecondActivity::class.java))
        }
    }

    override fun onStart() {
        super.onStart()

        //Register subscriber
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        //Unregister subscriber
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    @Subscribe(threadMode = MAIN)
    fun onInputTextEvent(event: InputTextEvent) {
        tvTextField.text = event.text
    }
}
