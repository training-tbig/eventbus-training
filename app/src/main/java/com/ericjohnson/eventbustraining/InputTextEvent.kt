package com.ericjohnson.eventbustraining


data class InputTextEvent(val text: String)