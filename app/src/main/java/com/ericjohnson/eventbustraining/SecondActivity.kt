package com.ericjohnson.eventbustraining

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.tvTextField
import kotlinx.android.synthetic.main.activity_second.tvResult
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode.MAIN
import org.greenrobot.eventbus.ThreadMode.POSTING

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)

        //remove all sticky events
        EventBus.getDefault().removeStickyEvent(InputTextEvent::class.java)
        super.onStop()
    }

    @Subscribe(sticky = true, threadMode = MAIN)
    fun onInputTextEvent(event: InputTextEvent) {
        tvResult.text = event.text
    }
}
